import pyautogui
from PIL import Image
from pytesseract import image_to_string
import os
from pynput import mouse
import sys

def on_click(x, y, button, pressed):
	if button == mouse.Button.left and pressed:
		raise Exception((x, y))

coords = []
print("First click is the top left of the box you want")
print("Second click is the bottom right of the box you want")
while(len(coords) < 4):
	with mouse.Listener(on_click=on_click) as listener:
		try: 
			listener.join()
		except Exception as e:
			print(e.args[0])
			coords.append(e.args[0][0])
			coords.append(e.args[0][1])


print(coords)
location = (coords[0], coords[1], coords[2]-coords[0], coords[3]-coords[1])
loop = True
while(loop):
	a = input('Press a key to take a screenshot')
	if (a == 'z'):
		loop = False
	
	image = pyautogui.screenshot('LOOKATME.png', region=location)
	
	try:
		filePath = os.path.join(os.getcwd(), "LOOKATME.png")
		i = Image.open(filePath)
		text = image_to_string(i, lang='eng')
		for char in text:
			try:
				print(char, end='')
			except:
				pass
	except Exception as e:
		print(e)

